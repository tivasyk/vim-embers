# vim-embers

мінімалістична тема для vim: монохромна чорно-біла з одноколірним червоним акцентом та помірним використанням текстових атрибутів (жирний, нахилений, підкреслений текст) для сематичного виділення. 

мета:

* схема має природньо інтегруватися з терміналом без додаткових налаштувань, тому повинна використовувати лише базові кольори (сім), їхні яскравіші відповідники, і «рідні» можливості терміналу з відображення жирного й нахиленого тексту;
* ...

натхнення й запозичення:

* колірна тема vim-bruin (основа для модифікації, використання атрибутів тексту)
* колірна тема vim-warlock (використання сірого для акцентування)
* [orange light](https://unsplash.com/photos/time-lapse-photography-of-orange-light-J40eheaQ_OE) (фото benjamin blättler, unsplash.com)
