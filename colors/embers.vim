" Name:         embers
" Description:  монохромна тема в віддінках сірого з типографічним виділенням та єдиним колірним
"               акцентом червого кольору (попіл та жар) і прозорим тлом; покладається на основні
"               кольори (ansi) задля кращої інтеграції з темою термінального емулятора.
" Author:       tivasyk <tivasyk@gmail.com>
" Website:      https://blog.malynka.duckdns.org
" Last Updated: 2024-02-20

hi clear
if exists("syntax_on")
    syntax reset
endif

let g:colors_name = 'embers'

" базові стилі
hi! link Normal             _Ash
" семантика й синтаксис
hi! link MatchParen         _Embers
hi! link Comment            _DarkAsh_i
hi! link Constant           _OldAsh
    hi! link String         _OldAsh_i
    hi! link Character      _WhiteAsh
    hi! link Number         _WhiteAsh
    hi! link Boolean        _OldAsh
    hi! link Float          _OldAsh
hi! link Identifier         _WhiteAsh
    hi! link Function       _WhiteAsh
hi! link PreProc            _OldAsh
hi! link Special            _Ash
hi! link Statement          _OldAsh
hi! link Todo               _OldAsh_r
hi! link Type               _Ash_bi
hi! link Error              _Glow
hi! link Underlined         _Ash_u
" елементи інтерфейсу
hi! link LineNr             _Coals
    hi! link LineNrAbove    _Coals
    hi! link LineNrBelow    _Coals
    hi! link CursorLineNr   _Ash
hi! link NonText            _Coals
    hi! link SpecialKey     _Coals
hi! link VertSplit          _Coals
hi! link Visual             _Ash_r
hi! link CursorLine         _Ash
    hi! link CursorColumn   _Transparent

" особливі налаштування для теми

let s:t_Co = has('gui_running') ? -1 : (&t_Co ?? 0)
if (has('termguicolors') && &termguicolors) || has('gui_running')
    " графічний інтерфейс або повноколірний емулятор терміналу
    if &background ==# 'dark'
        " стилі для темного тла
        hi _Transparent     guifg=NONE guibg=NONE gui=NONE cterm=NONE
        hi _Coals           guifg=#505050 guibg=NONE gui=NONE
        hi _Coals_bi        guifg=#505050 guibg=NONE gui=bold,italic cterm=bold,italic
        hi _DarkAsh         guifg=darkgrey guibg=NONE gui=NONE
        hi _DarkAsh_i       guifg=darkgrey guibg=NONE gui=italic cterm=italic
        hi _Ash             guifg=lightgrey guibg=NONE gui=NONE
        hi _Ash_i           guifg=lightgrey guibg=NONE gui=italic cterm=italic
        hi _Ash_u           guifg=lightgrey guibg=NONE gui=underline cterm=underline
        hi _Ash_bi          guifg=lightgrey guibg=NONE gui=bold,italic cterm=bold,italic
        hi _Ash_r           guifg=black guibg=grey gui=NONE cterm=NONE
        hi _OldAsh          guifg=white guibg=NONE gui=NONE
        hi _OldAsh_i        guifg=white guibg=NONE gui=italic cterm=italic
        hi _OldAsh_r        guifg=white guibg=NONE gui=underline cterm=underline
        hi _WhiteAsh        guifg=white guibg=NONE gui=bold cterm=bold
        hi _WhiteAsh_i      guifg=white guibg=NONE gui=bold,italic cterm=bold,italic
        " кольоровий акцент
        hi _Embers          guifg=red guibg=NONE gui=bold cterm=bold
        hi _Glow            guifg=red guibg=white gui=bold,reverse cterm=bold,reverse
    else
        " стилі для світлого тла
        hi _Ash guifg=grey guibg=NONE gui=NONE cterm=NONE
    endif
    unlet s:t_Co
    finish

elseif s:t_Co >= 256
    " багатоколірний термінал
    if &background ==# 'dark'
        " стилі для темного тла
        hi _Ash ctermfg=grey ctermbg=NONE cterm=NONE
    else
        " стилі для світлого тла
        hi _Ash ctermfg=grey ctermbg=NONE cterm=NONE
    endif
    unlet s:t_Co
    finish

elseif s:t_Co >= 16
    " термінал 256 кольорів
    if &background ==# 'dark'
        " стилі для темного тла
        hi _Ash ctermfg=grey ctermbg=NONE cterm=NONE
    else
        " стилі для світлого тла
        hi _Ash ctermfg=grey ctermbg=NONE cterm=NONE
    endif
    unlet s:t_Co
    finish

elseif s:t_Co >= 8
    " термінал 16 кольорів
    if &background ==# 'dark'
        " стилі для темного тла
        hi _Ash ctermfg=grey ctermbg=NONE cterm=NONE
    else
        " стилі для світлого тла
        hi _Ash ctermfg=grey ctermbg=NONE cterm=NONE
    endif
    unlet s:t_Co
    finish

else
    " ймовірно, простий термінал без підтримки кольороів
    hi _Ash term=NONE
endif

